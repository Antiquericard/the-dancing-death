﻿using UnityEngine;
using System.Collections;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class StopifyterSpawner : Singleton<StopifyterSpawner> {
	public Stopifyter[] pbStopifyter;
	public Transform startPoint;
	public float rithmRate = 0.1f;
	public float lastTimePlayerDancingRateToChase = 0.5f; //segundos desde que el player bailó por última vez para perseguir al player
	public float lastTimePlayerPayedRateToChase = 5f; //segundos desde que el player pagó a otro stopifyter para perseguir al player
	public int maxSpawnedWhileAlive = 1;
	public int maxSpawnedAllowed = 9;
	public float initialWaitTime=8;
	public float xDistanceFromPlayerStartSpawnPos = 10f;



	public int totalSpawned = 0;
	public int totalAlive = 0;
	private float lastTimePlayerPaid = 0;

	protected override void Awake ()
	{
		base.Awake ();
	}
	public void Start(){

	}

	public void Update(){
		if ( GameManager.Instance.timer >= initialWaitTime
			&& totalSpawned < maxSpawnedAllowed && totalAlive < maxSpawnedWhileAlive
			//haya pasado el tiempo desde que se bailó por última vez
			&& (GameManager.Instance.timer - GameManager.Instance.lastTimeDancing >= lastTimePlayerDancingRateToChase)
			//haya pasado el tiempo desde que se pagó a otro stopifyter
			&& (GameManager.Instance.timer - lastTimePlayerPaid >= lastTimePlayerPayedRateToChase)
		) {
			spawn ();
		}
	}

	private void spawn(){
		totalSpawned++;
		totalAlive++;

		Stopifyter s = (Stopifyter)Instantiate(pbStopifyter[Random.Range (0, 1)]);
		Vector3 playerPos = GameManager.Instance.Player.transform.position;
		s.transform.position = new Vector3 (playerPos.x-xDistanceFromPlayerStartSpawnPos, playerPos.y, playerPos.z);
		((DancingSoundManager)DancingSoundManager.Instance).playChase ();
	}

	public void killed(){
		totalAlive--;
		((DancingSoundManager)DancingSoundManager.Instance).play ();
	}

	public void playerPaid(){
		lastTimePlayerPaid = GameManager.Instance.timer;
	}
}

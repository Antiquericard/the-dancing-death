﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class AIMovementController : CharacterMovementController {


	[SerializeField]
	private List<string> triggeredTags = new List<string>(){"Player"};

	[SerializeField]
	float rotationSpeed = 3;

	[SerializeField]
	protected float chaseRange = 10;

	[SerializeField]
	float minDistanceFromPlayerFollowing = 2;

	public Transform target;

	protected override void Start ()
	{
		base.Start ();
		target = GameManager.Instance.Player.transform;
		//animacion idle
	}

	public override void InputMovement ()
	{
	}

	public override void MoveCharacter ()
	{
		//_destination = target.position;

		//_shouldMove =  Vector3.Distance (transform.position, _destination) >= chaseRange;
//		Debug.Log (Vector3.Distance (transform.position, _destination));
		base.MoveCharacter ();
	}

//	public virtual void FixedUpdate(){
//		if (target != null) {
////			float range = Vector3.Distance (transform.position, target.position);
////			if (range <= chaseRange) {
//				//animacion running
//
////				transform.position = Vector3.MoveTowards (transform.position, target.position, CharacterSpeed *Time.fixedDeltaTime);
//				
//			Vector3 dir = target.position - transform.position;
//			Vector3 movementDistance = dir.normalized * CharacterSpeed * Time.deltaTime;
//			transform.position += movementDistance;
//
////				transform.position=Vector3.MoveTowards(transform.position, target.position, minDistanceFromPlayerFollowing) * CharacterSpeed * Time.deltaTime;
////			}
//		} else {
//			//animacion idle
//		}
//	}


	public virtual void targetInRadio(){

	}
	public virtual void targetOutOfRadio(){

	}
}

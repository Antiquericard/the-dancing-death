﻿using UnityEngine;
using System.Collections;
using MoreMountains.CorgiEngine;

public class Stopifyter : AIMovementController {
	

	public int coinsToRequest = 10;
	public float maxTimeToWaitPaying = 5;
	public float minDistanceToStopPlayer=1.5f;

	private bool requestingPaying=false;
	private float initialReqTime = 0;

    private bool playerHasPay = false;


    protected override void Start ()
	{
		base.Start ();
	}
    public override void MoveCharacter()
    {   
		if (!playerHasPay)
			_destination = target.position;
		else {
			_destination = StopifyterSpawner.Instance.startPoint.position;

			if (Vector3.Distance (GameManager.Instance.Player.transform.position, transform.position) >= 10
				&& Vector3.Distance (transform.position, _destination) <= 10f)
				Destroy (gameObject);
		}
		
        _shouldMove = Vector3.Distance(transform.position, _destination) >= chaseRange;

        base.MoveCharacter();
    }
    protected override void Update ()
	{
		if (GameManager.Instance.Player.isDead)
			return;

        MoveCharacter();

        AnimateCharacter();

		//if player does not pay
		if (!playerHasPay) {
	        //request paying
	        if (!requestingPaying && playerIsInRangeToRequestPay()) {
				requestPlayerPays ();
			}

			//kill player 
			if (requestingPaying && GameManager.Instance.timer - initialReqTime >= maxTimeToWaitPaying) {
				GameManager.Instance.Player.Kill ();
				Debug.Log ("Killing player");
			}
			if (requestingPaying) {
				if (Input.GetKeyDown (KeyCode.B) && GameManager.Instance.money >= coinsToRequest) {
					payed ();
				}
			}
		}
	}

	public bool playerIsInRangeToRequestPay(){
        return Vector3.Distance (transform.position, GameManager.Instance.Player.transform.position) <= minDistanceToStopPlayer;
	}

	public override void AnimateCharacter ()
	{
		base.AnimateCharacter ();
		if(!CharacterIsFacingRight)
			transform.GetComponentInChildren<RectTransform> ().rotation = Quaternion.Euler(new Vector3(0, 180, 0));
	}
		
	public void requestPlayerPays(){
		requestingPaying = true;

		//iniciar contador para esperar un max a que le pague
		initialReqTime = GameManager.Instance.timer;

//		Debug.Log ("Pagaaaa Stopify Premium!!!");
    }

	public void payed(){
        //descontar dinero del player
        GameManager.Instance.AddPoints(-coinsToRequest);
        playerHasPay = true;

		//matar al stopifyter
		StopifyterSpawner.Instance.killed ();

		//avisar al manager del pago
		StopifyterSpawner.Instance.playerPaid();

//		Debug.Log ("Stopify pagado");
	}
}
﻿using UnityEngine;
using System.Collections;
using MoreMountains.CorgiEngine;

public class DancingSoundManager : SoundManager {
	public AudioClip[] clips;
	public AudioClip[] chaseClips;

	private AudioSource source;

	protected override void Awake ()
	{
		base.Awake ();
		source = GetComponent<AudioSource> ();
	}

	public void Start(){
		play ();
	}

	public void play(int index=0){
		source.clip = clips [index];
		PlayBackgroundMusic (source);
	}

	public void playChase(int index=0){
		source.clip = chaseClips [index];
		PlayBackgroundMusic (source);
	}
}

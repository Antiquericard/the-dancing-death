﻿using UnityEngine;
using System.Collections;

public class DancingBeatDetManager : MonoBehaviour {
	public BeatDetection audioBeat;
	public GameObject[] genergy;
	public GameObject[] gkick;
	public GameObject[] gsnare;
	public GameObject[] ghithat;

	public string genergyTag = "Energy";
	public string gkickTag = "Kick";
	public string gsnareTag = "Snare";
	public string ghithatTag ="HitHat";

//	public Material matOn;
//	public Material matOff;
	private Vector3[] genergyInitScale;
	private Vector3[] gkickInitScale;
	private Vector3[] gsnareInitScale;
	private Vector3[] ghithatInitScale;

	// Use this for initialization
	void Awake () {
		//Register the beat callback function
		if (audioBeat != null)
			audioBeat.CallBackFunction = MyCallbackEventHandler;
		else {
			audioBeat = FindObjectOfType<BeatDetection> ();

			if (audioBeat != null) {
				audioBeat.CallBackFunction = MyCallbackEventHandler;
			}
		}
			

		//find objetcs
		genergy = GameObject.FindGameObjectsWithTag (genergyTag);
		gkick = GameObject.FindGameObjectsWithTag (gkickTag);
		gsnare = GameObject.FindGameObjectsWithTag (gsnareTag);
		ghithat = GameObject.FindGameObjectsWithTag (ghithatTag);


		if (genergy != null && genergy.Length > 0) {
			genergyInitScale = new Vector3[genergy.Length];
			loadInitialScale (genergy, BeatDetection.EventType.Energy);
		}
		if (gkick != null && gkick.Length > 0) {
			gkickInitScale = new Vector3[gkick.Length];
			loadInitialScale (gkick, BeatDetection.EventType.Kick);
		}
		if (gsnare != null && gsnare.Length > 0) {
			gsnareInitScale = new Vector3[gsnare.Length];
			loadInitialScale (gsnare, BeatDetection.EventType.Snare);
		}
		if (ghithat != null && ghithat.Length > 0) {
			ghithatInitScale = new Vector3[ghithat.Length];
			loadInitialScale (ghithat, BeatDetection.EventType.HitHat);
		}
	}

	private void loadInitialScale(GameObject[] objects, BeatDetection.EventType type){
		for(int i=0; i<objects.Length; i++) {
			if (objects [i] != null) {
				switch (type) {
				case BeatDetection.EventType.Energy:
					genergyInitScale [i] = objects [i].transform.localScale;
					break;
				case BeatDetection.EventType.HitHat:
					ghithatInitScale [i] = objects [i].transform.localScale;
					break;
				case BeatDetection.EventType.Kick:
					gkickInitScale [i] = objects [i].transform.localScale;
					break;
				case BeatDetection.EventType.Snare:
					gsnareInitScale [i] = objects [i].transform.localScale;
					break;
				}
			}
		}
	}

	/// <summary>
	/// Changes the type of the scale all objects of.
	/// If scaleMultiplier is equals to 1: we assing de initial Scale of object
	/// Otherwise we scale the object by this scaleMultiplier parameter.
	/// </summary>
	/// <param name="type">Type.</param>
	/// <param name="scaleMultiplier">Scale multiplier.</param>
	private void changeScaleAllObjectsOfType(BeatDetection.EventType type, float scaleMultiplier = 1.01f){
		GameObject[] objects = null;
		Vector3[] initialScales = null;

		switch(type)
		{
		case BeatDetection.EventType.Energy:
			objects = genergy;
			initialScales = genergyInitScale;
			break;
		case BeatDetection.EventType.HitHat:
			objects = ghithat;
			initialScales = ghithatInitScale;
			break;
		case BeatDetection.EventType.Kick:
			objects = gkick;
			initialScales = gkickInitScale;
			break;
		case BeatDetection.EventType.Snare:
			objects = gsnare;
			initialScales = gsnareInitScale;
			break;
		}

		for(int i=0; i<objects.Length; i++) {
			if (objects [i] != null) {
				if (scaleMultiplier != 1)
					objects [i].transform.localScale *= scaleMultiplier;
				else
					objects [i].transform.localScale = initialScales [i];
			}
		}
	}


	public void MyCallbackEventHandler(BeatDetection.EventInfo eventInfo)
	{
		StartCoroutine (animate(eventInfo.messageInfo));
	}

	private IEnumerator animate(BeatDetection.EventType type)
	{
		//apply an scale
		changeScaleAllObjectsOfType (type);
		yield return new WaitForSeconds (0.05f * 3);

		//reset to the initial scale
		changeScaleAllObjectsOfType (type, 1);
		yield break;
	}

}

﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class DancingPlayer : MonoBehaviour
{

    /// the current health of the character
    [SerializeField]
    public float rythm=0;
    [SerializeField]
    public float MaxRythm=1;

    [SerializeField]
    public bool isDead = false;

    public CharacterMovementController movementController { get; private set; }
    public Animator anim;
    void Awake()
    {
        movementController = GetComponent<CharacterMovementController>();
        rythm = 0;
        isDead = false;
        animState_=animState.none;

    }

    public void addRythm(bool add)
    {
        if (add)
        {
            rythm += 1f /(float)GameManager.Instance.zombieCount;
        }
        else
        {
            rythm -= 1f / (float)GameManager.Instance.zombieCount;
        }
       
        rythm = Mathf.Clamp(rythm, 0, MaxRythm);
    }
    /// <summary>
    /// Makes the player respawn at the location passed in parameters
    /// </summary>
    /// <param name="spawnPoint">The location of the respawn.</param>
    public virtual void RespawnAt(Transform spawnPoint)
    {
        // we make sure the character is facing right
        if (!movementController.CharacterIsFacingRight)
        {
            movementController.Flip();
        }
        // we raise it from the dead (if it was dead)
        // BehaviorState.IsDead = false;
        // we re-enable its 2D collider
        GetComponent<Collider2D>().enabled = true;
        isDead = false;
        // we make it handle collisions again
        // _controller.CollisionsOn();
        transform.position = spawnPoint.position;
        //rythm = MaxRythm;
    }

    /// <summary>
    /// Called to disable the player (at the end of a level for example. 
    /// It won't move and respond to input after this.
    /// </summary>
    public virtual void Disable()
    {
        enabled = false;

        GetComponent<Collider2D>().enabled = false;
    }
    public void dance(animState st,bool active)
    {
       
        animState_ = st;
    }
    void Update()
    {

        if (LastanimState_  != animState_)
        {
            //MMAnimator.UpdateAnimatorBool(anim, "country", false);
            //MMAnimator.UpdateAnimatorBool(anim, "disco", false);
            //MMAnimator.UpdateAnimatorBool(anim, "metal", false);
            //MMAnimator.UpdateAnimatorBool(anim, "electro", false);
            
            MMAnimator.UpdateAnimatorBool(anim, System.Enum.GetName(typeof(animState), LastanimState_), false);
            if(animState_!=animState.none)
            MMAnimator.UpdateAnimatorBool(anim, System.Enum.GetName(typeof(animState),animState_), true);
            LastanimState_ = animState_;
            if (!movementController.CharacterIsFacingRight)
            {
                movementController.Flip();
            }
        }


    }
    public enum animState{ electro,metal, country, disco,none };
    public animState animState_=animState.none;
    private animState LastanimState_=animState.none;
    /// <summary>
    /// Kills the character
    /// </summary>
    public virtual void Kill()
    {
        isDead = true;
        rythm = 0;
       
        GameManager.Instance.Pause();
        GUIManager.Instance.SetDeadScreen(true);
        // GameManager.Instance.FreezeCharacter();
    }
}


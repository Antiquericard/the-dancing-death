﻿using UnityEngine;
using System.Collections;
using MoreMountains.CorgiEngine;

public class LayerChanger : MonoBehaviour
{
    bool stay = false;
    bool enter = false;
    public SpriteRenderer sp;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "Player")
            return;
        sp.sortingOrder = 3;
        //enter = true;
        //if (stay)
        //{
        GameManager.Instance.Player.movementController.to *= -2f;
        GameManager.Instance.Player.movementController.SetHorizontalMove(GameManager.Instance.Player.movementController._horizontalMove*-1);
        GameManager.Instance.Player.movementController.SetVerticalMove(GameManager.Instance.Player.movementController._verticalMove * -1);/*;*/
        GameManager.Instance.CanMove =  false;
           // stay = false;
           // enter = false;
            
        //}

    }

    IEnumerator delayInput()
    {
        yield return new WaitForSeconds(0.3f);
        GameManager.Instance.CanMove = true;
       
    }
    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "Player")
            return;
        sp.sortingOrder = 0;
        StartCoroutine(delayInput());
        //if(enter)
        //stay = false;
        //enter = false;

    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "Player")
            return;
        sp.sortingOrder = 3;
        //enter = true;
        //if (stay)
        //{
        //GameManager.Instance.Player.movementController.to *= -2f;
        //GameManager.Instance.Player.movementController.SetHorizontalMove(GameManager.Instance.Player.movementController._horizontalMove * -1);
        //GameManager.Instance.Player.movementController.SetVerticalMove(GameManager.Instance.Player.movementController._verticalMove * -1);/*;*/
        GameManager.Instance.CanMove = false;
        // stay = false;
        // enter = false;

        //}
    }


}

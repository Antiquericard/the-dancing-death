﻿using UnityEngine;
using System.Collections;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class DancingZombie : DialogueZone
{
    [Header("Key to react")]
    public KeyCode key;
    [Header("Zombies Rythm")]
    public Color initialColor = Color.gray;
    public SpriteRenderer sp;
    public float rythm = 0;
    public float maxRythm = 1;

    public float rythmCharge = 0.01f;

    public bool rythmIsCharged = false;
    public bool decreaseWithOutPlayer = false;
    public Animator _animator;
    public DancingPlayer.animState danceAnimation = DancingPlayer.animState.disco;
    bool animationActive = false;
    bool playerIsDancing = false;
    void Awake()
    {
        sp.color = initialColor;
        _animator = transform.parent.GetComponent<Animator>();
        sp = transform.parent.GetComponent<SpriteRenderer>();
    }
    void Undancinplayer()
    {

    }

    public void FixedUpdate()
    {
        if ((inContact && !rythmIsCharged) || decreaseWithOutPlayer)
        {
            if (Input.GetKeyDown(key) && inContact)
            {
                GameManager.Instance.Player.dance(danceAnimation, true);
            }
        }

    }
    void Update()
    {
        if ((inContact && !rythmIsCharged) || decreaseWithOutPlayer)
        {
            if (Input.GetKeyDown(key) && inContact)
            {
                GameManager.Instance.Player.dance(danceAnimation, true);
            }



            if (Input.GetKeyDown(key) && !decreaseWithOutPlayer)
            {

                addRythm(rythmCharge);

                //bailaaa
                //if (!playerIsDancing)
                //{
                //    playerIsDancing = true;
                //    Undancinplayer();
                //    GameManager.Instance.Player.dance(danceAnimation, false);
                //}
                GameManager.Instance.lastTimeDancing = GameManager.Instance.timer;
                if (!animationActive)
                {
                    MMAnimator.UpdateAnimatorBool(_animator, "dance", true);
                }
            }
            else
            {
                addRythm(-(rythmCharge / 25));
                //  GameManager.Instance.Player.dance(DancingPlayer.animState.none, false);
                Undancinplayer();
            }
        }
        if (rythm == maxRythm && !rythmIsCharged)
        {
            GameManager.Instance.Player.addRythm(true);
            rythmIsCharged = true;
        }

        if (rythmIsCharged)
        {
            sp.color = Color.white;
        }
        else
        {
            sp.color = initialColor;
        }
    }
    protected override void OnTriggerStay2D(Collider2D collision)
    {
        base.OnTriggerStay2D(collision);

    }
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
        decreaseWithOutPlayer = false;
        addRythm(-(rythmCharge / 50));
        if (rythmIsCharged)
            GameManager.Instance.Player.addRythm(false);
        rythmIsCharged = false;

    }

    protected override void OnTriggerExit2D(Collider2D collision)
    {
        if (!rythmIsCharged)
        {
            decreaseWithOutPlayer = true;
            animationActive = false;
            MMAnimator.UpdateAnimatorBool(_animator, "dance", false);
        }

        inContact = false;
        GameManager.Instance.Player.dance(DancingPlayer.animState.none, false);

        base.OnTriggerExit2D(collision);

    }

    public void addRythm(float val)
    {
        rythm += val;
        rythm = Mathf.Clamp(rythm, 0, maxRythm);
        // GameManager.Instance.Player.dance(danceAnimation, true);
    }

}

﻿using UnityEngine;
using System.Collections;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class CharacterMovementController : MonoBehaviour {
    public float CharacterSpeed;
    public Transform StartingPoint;
    public bool CharacterIsFacingRight = true;

    public float _horizontalMove;
    public float _verticalMove;
    protected float _threshold = 0.0001f;
    protected Vector3 _offset;
    protected Vector3 _positionLastFrame;
    public float _currentSpeed = 0f;

    protected Rigidbody2D _rigidbody2D;
    protected BoxCollider2D _boxCollider2D;
    protected Animator _animator;

    public Vector3 to = Vector3.right;
    public Vector3 _destination = Vector3.zero;

    public bool _shouldMove { get; set; }

    /// <summary>
    /// Initialization
    /// </summary>
    protected virtual void Start()
    {
        // we get our various components
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _boxCollider2D = GetComponent<BoxCollider2D>();
        _animator = GetComponent<Animator>();

        // we define the offset based on the distance between our object's position and the center of the boxcollider
        _offset = _boxCollider2D.bounds.center - transform.position;

        _shouldMove = true;
        // we move our character to its starting point
        if (StartingPoint == null)
        {
            StartingPoint = LevelManager.Instance.getSpawnPoint();
        }
        transform.position = StartingPoint.position - _offset;

    }

    /// <summary>
    /// On update, we get the input, decide if we should move or not, and if needed move the character and animate it
    /// </summary>
    protected virtual void Update()
    {
        InputMovement();

        MoveCharacter();

        AnimateCharacter();

    }

    /// <summary>
    /// Handles input and decides if we can move or not
    /// </summary>
    /// <param name="movement">Movement.</param>
    public virtual void InputMovement()
    {
        if (GameManager.Instance.Player.isDead)
            return;
        // we get both direction axisif (!GameManager.Instance.CanMove || _player==null) { return; }
        if (InputManager.Instance != null && GameManager.Instance.CanMove)
        {
            if (!InputManager.Instance.IsMobile)
            {
                _horizontalMove = Input.GetAxis("Horizontal1");
                _verticalMove = Input.GetAxis("Vertical1");
              
            }
        }

        if (!GameManager.Instance.CanMove && !_shouldMove&& (Input.GetAxis("Horizontal1")!=0 || Input.GetAxis("Vertical1")!=0))
        {
            GameManager.Instance.CanMove = true;
            _shouldMove = true;
        }
        //to = Vector3.zero;
        if (_horizontalMove > _threshold)
        {
            to = Vector3.right;
        }
        if (_horizontalMove < -_threshold)
        {
            to = Vector3.left;
        }
        if (_verticalMove > _threshold)
        {
            to = Vector3.up;
        }
        if (_verticalMove < -_threshold)
        {
            to = Vector3.down;
        }
        _destination = transform.position + to;
    }

    /// <summary>
    /// Moves the character to the set destination
    /// </summary>
    public virtual void MoveCharacter()
    {
        if (!_shouldMove) { return; }
        transform.position = Vector3.MoveTowards(transform.position, _destination, Time.deltaTime * CharacterSpeed);
    }

    /// <summary>
    /// Animates the character if it has an animator set
    /// Also flips the character if needed
    /// </summary>
    public virtual void AnimateCharacter()
    {
        //         if (_destination == null)
        //         { return; }

        if (_destination.x - _offset.x < transform.position.x)
        {
            if (CharacterIsFacingRight)
            {
                Flip();
            }
        }
        if (_destination.x - _offset.x > transform.position.x)
        {
            if (!CharacterIsFacingRight)
            {
                Flip();
            }
        }

        // if we have an animator we'll want to let it know if our character is moving or not
        if (_animator != null)
        {
            // if we've moved since last frame, we haven't reached our destination yet, so we set our current speed at 1
            if (_positionLastFrame != transform.position)
            {
                _currentSpeed = 1f;
            }
            else
            {
                // if we haven't moved last frame, we're static
                _currentSpeed = 0f;
            }
            // we pass that parameter to our animator
            MMAnimator.UpdateAnimatorFloat(_animator, "Speed", _currentSpeed);
        }

        _positionLastFrame = transform.position;
    }



    /// <summary>
    /// Flip the character horizontally
    /// </summary>
    public virtual void Flip()
    {
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        CharacterIsFacingRight = transform.localScale.x > 0;
    }

    /// <summary>
    /// When the jump button is pressed
    /// </summary>
    protected virtual void ButtonPressed()
    {

    }

    public virtual void SetHorizontalMove(float value)
    {
        _horizontalMove = value;

    }

    public virtual void SetVerticalMove(float value)
    {
        _verticalMove = value;
    }


}
